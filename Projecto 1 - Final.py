#IST - LEIC-A - Project de FP - Metodo D'Hondt
#Grupo - AL018
#Tiago Miguel Calhanas Goncalves - no. 83567

def mandatos (nr_mandatos, nr_votos):
    """|mandatos(tuplo, tuplo)

|Recebe dois argumentos, o primeiro com o numero de mandatos que tem de\n di\
struibuir e em segundo o numero de votos de cada partido.
|Sao devolvidos os numeros de mandatos correspondentes a cada partido (na mesma\
\nordem) divididos pelo Metodo D'Hondt."""
    div = [2] * len(nr_votos)
    lsmandatos = [0] * len(nr_votos)
    #Cria-se uma lista com os votos para facilitar a modificacao de dados e nao
    #perder os valores iniciais.
    lsvotos = list(nr_votos)
    #Vai repetir-se para cada mandato que houver por atribuir e vai
    #verificar qual o maximo da lista de votos.
    for i in range(nr_mandatos):
        conta = []
        maximo = max(lsvotos)
        #Percorre todos os elementos da lista que contem os votos e verifica se
        #e' o maximo, se for guarda o indice desse elemento na lista conta.
        for e in range(len(lsvotos)):
            if maximo == lsvotos[e]:
                conta = conta + [e]
        #Se houver mais do que um maximo, o ciclo verifica qual desses
        #corresponde ao que tem menos mandatos, eliminando os que te'm mais
        #mandatos sucessivamente, ate' apenas haver um.
        if len(conta) > 1:
            while len(conta) != 1:
                if lsmandatos[conta[0]] <= lsmandatos[conta[1]]:
                    del conta[1]
                else:
                    del conta[0]
        #Adiciona um mandato 'a lista no mesmo indice correspondente ao numero
        #de votacoes (lista conta), divide as votacoes (originais) pelo seu
        #divisor (que esta no mesmo indice) colocando na lista de votos e depois
        #incrementa o divisor numa unidade.
        lsmandatos[conta[0]] = lsmandatos[conta[0]] + 1
        lsvotos[conta[0]] = nr_votos[conta[0]]/div[conta[0]]
        div[conta[0]] = div[conta[0]] + 1
    return tuple(lsmandatos)


def assembleia (votacoes):
    """|assembleia(tuplo)

|Recebe um argumento contendo um tuplo com todas as votacoes de todos os\n\
circulos eleitorais.
|Distribui todos os mandatos usando a funcao mandatos aplicando cada elemento\n\
 do tuplo (que contem as votacoes do cicurlo correspondente),por todos os part\
 idos."""
    nr_mandatos = (16,3,19,3,4,9,3,9,4,10,47,2,39,9,18,6,5,9,5,6,2,2)
    total_mandatos = [0] * len(votacoes[0])
    #Para cada circulo eleitoral, vai fazer uso da funcao mandatos e distribuir
    #os mandatos atribuidos consoante os votos.
    for i in range(len(nr_mandatos)):
        tuplo = mandatos(nr_mandatos[i], votacoes[i])
        for e in range(len(tuplo)):
            total_mandatos[e] = total_mandatos[e] + tuplo[e]
    return tuple(total_mandatos)


def max_mandatos (votacoes):
    """|max_mandatos(tuplo)

    |Recebe um unico argumento que e' um tuplo que contem todas\n as votacoes de 
    todos os ciruculos eleitorais.
|Faz uso da funcao assembleia e consequentemente da mandatos e devolve qual\n\
 dos partidos e' que tem maior numero de mandatos em assembleia ou seja \n\
 aquele que ganhou.
|Em caso de empate a funcao em vez de devolver o vencedor devolve a mensagem\n\
'Empate tecnico'.
"""
    mandatos_totais =  assembleia(votacoes)
    candidatos = ['PDR\tPartido Democratico Republicano','PCP-PEV\tCDU - Coliga\
cao Democratica Unitaria','PPD/PSD-CDS/PP\tPortugal a Frente','MPT\tPartido da \
Terra','L/TDA\tLIVRE/Tempo de Avancar','PAN\tPessoas-Animais-Natureza','PTP-MAS\
\tAgir','JPP\tJuntos Pelo Povo','PNR\tPartido Nacional Renovador','PPM\tPartid\
o Popular Monarquico','NC\tNos, Cidadaos!','PCTP/MRPP\tPartido Comunista dos Tr\
abalhadores Portugueses','PS\tPartido Socialista','B.E.\tBloco de Esquerda','PU\
RP\tPartido Unidos dos Reformados e Pensionistas']
    conta = ()
    mandatos_max = max(mandatos_totais)
    #Usa funcao assembleia, que distribui todos os mandatos. Verifica qual o
    #partido que teve o maior numero de mandatos, percorrendo todos os elementos
    #que se forem o seu indice fica guardado na lista conta.
    for i in range(len(mandatos_totais)):
        if mandatos_totais[i] == mandatos_max:
            conta = conta + (i,)
    #Se houver mais do que um maximo, ou seja o comprimento da lista conta e'
    #maior do que um, e' devolvida a mensagem 'Empate tecnico', senao usa o 
    #indice da lista para devolver o partido que ganhou.
    if len(conta) > 1:
        return('Empate tecnico')
    else:
        return(candidatos[conta[0]])
